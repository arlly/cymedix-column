<?php

namespace App\Listeners;

use App\Events\LogGetJson;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class LogGetJsonConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LogGetJson  $event
     * @return void
     */
    public function handle(LogGetJson $event)
    {
        //
    	
    	
    	//Event
    	
    	\Log::info("Json取得完了");
    	
    }
}
