<?php

namespace App\Listeners;

use App\Events\UserWasRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailUserRegisterConfirmation 
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserWasRegistered  $event
     * @return void
     */
    public function handle(UserWasRegistered $event)
    {
    	$user = $event->user;
    	//print_r($user); exit();
    	$result = $user->first();
        //
    	\Mail::send('mail.registEmail', ['result' => $result], function($message) use ($result){	
    		$message->from(config('mail.from.address'), config('mail.from.name'));
    		$message->to(config('mail.from.address'))->subject('ユーザーが登録されました');
    	});
    }
}
