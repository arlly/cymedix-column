<?php

namespace App\Listeners;

use App\Events\EmailGetJson;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailGetJsonConfirmation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmailGetJson  $event
     * @return void
     */
    public function handle(EmailGetJson $event)
    {
        //
    }
}
