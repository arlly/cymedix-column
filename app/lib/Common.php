<?php
namespace app\lib;

class Common {

	/**
	 * 例外
	 */
	public static function Exception($prm=null, $line = 0) {
		$_SESSION["err"]["time"] = date("Y/m/d H:i:s");
		$_SESSION["err"]["code"] = $prm;
		$_SESSION["err"]["line"] = $line;

		header("Location: ". URL_HTTP. "error.php");
		exit();
	}

	// **************************************************
	// サニタイジング
	// **************************************************
	public static function ez_Sanitize($input){

		$input = strip_tags(htmlspecialchars($input, ENT_QUOTES));

		return $input;
	}

	/**
	 * 消費税を加算
	 */
	public static function addTax($prm) {
		$tax   = 0.05;
		$price = $prm;

		$add_tax_price = $price + ceil($price * $tax);
		return $add_tax_price;
	}

	// **************************************************
	// 配列データを一括変換
	// **************************************************
	public static function cnvFormstr($array) {

		foreach($array as $k => $v){
			// 「magic_quotes_gpc = On」のときはエスケープ解除
			if (get_magic_quotes_gpc()) {
				$v = stripslashes($v);
			}
			if (isset($array["act"])) {
				if ($array["act"] != "kan") {
					$v = htmlspecialchars($v);
				}
			}
			$array[$k] = $v;
		}
		return $array;
	}



	// **************************************************
	// チェック関連
	// **************************************************
	/*半角数字のみかチェック*/
	public static function chkNumber($data){

		$pat = "^[0-9]+$";

		if(ereg($pat, $data)){
			return TRUE;
		}else{
			return FALSE;
		}

	}

	/*半角かどうか調べる*/
	public static function chkHankaku($data){

		if (preg_match("/^[a-zA-Z0-9]+$/",$data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*全角カタカナかどうか調べる*/
	public static function chkKatakana($data){

		if (preg_match("/^(\xe3(\x82[\xa1-\xbf]|\x83[\x80-\xb6]|\x83\xbc))*$/",$data)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	/*メールアドレスの正当性を調べる*/
	public static function chkEmail($data) {

		//      if (preg_match('/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/',$data)) {
		if (preg_match('/^[a-zA-Z0-9_\.\-\:\,]+?@[A-Za-z0-9_\.\-]+$/',$data)) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	/*日付の正当性*/
	public static function chkDate($m, $d, $y){
		if(checkdate($m, $d, $y)){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	

	// **************************************************
	// 文字コード
	// **************************************************
	public static function strEncode($str, $code){

		switch($code){

			case("EUC"):
				$return_str = mb_convert_encoding($str,   "EUC-JP",   "UTF-8");
				break;

			case("UJIS"):
				$return_str = mb_convert_encoding($str,   "UTF-8",   "EUC-JP");
				break;

			case("SJIS"):
				$return_str = mb_convert_encoding($str,   "SJIS",   "UTF-8");
				break;

			case("SJIS2"):
				$return_str = mb_convert_encoding($str,   "SJIS",   "EUC-JP");
				break;

			case("SJIS3"):
				$return_str = mb_convert_encoding($str,   "SJIS",   "UTF-8");
				break;
		}

		return $return_str;
	}
	// **************************************************
	// データをMySQL用にクォート
	// **************************************************
	public static function quoteSmart($value){

		//if (!is_numeric($value)) {                    // 数値以外をクオートする
		$value   = "'" . mysql_real_escape_string($value) . "'";
		//}
		return $value;
	}

	// **************************************************
	// 文字列を任意の値にカット
	// **************************************************
	public static function cutString($prm, $max) {
		if (mb_strlen($prm, "UTF-8") > $max)
			$prm = mb_substr($prm, 0, $max, "UTF-8"). "・・・";

			return $prm;
	}

	// **************************************************
	// ランダムの指定文字数の英数字の文字列を取得
	// **************************************************
	public static function getRandomWord($num) {
		//使用する文字
		$char = '1234567890abcdefghijklmnopqrstuvwxyz';

		$charlen = mb_strlen($char);
		$result = "";
		for($i=1;$i<=$num;$i++){
			$index = mt_rand(0, $charlen - 1);
			$result .= mb_substr($char, $index, 1);
		}
		return $result;
	}

	// **************************************************
	// 改行文字の入った文字列を配列に変換
	// **************************************************
	public static function getCRWordToArray($word) {
		$word = trim($word);
		$replace_cr = array("\r\n" , "\r");
		$word = str_replace($replace_cr, "\n", $word);
		$result = explode("\n" , $word);
		return $result;
	}


}
