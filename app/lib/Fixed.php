<?php
namespace app\lib;

class Fixed {

    /**
     * 現在の職業
     */
    public static function nowJob()
    {
    	return array(
    			'' => "",
    			1 => "高校生",
    			2 => "大学生(院生)",
    			3 => "専門学生",
    			4 => "主婦",
    			5 => "会社員(飲食業界)",
    			6 => "会社員(飲食業界外)",
    			7 => "自営(飲食業界)",
    			8 => "自営(飲食業界外)",
    			9 => "フリーター(飲食業界)",
    			10 => "フリーター(飲食業界外)",
    			11 => "就職活動中",
    			12 => "その他",
    	);
    }
    
    /**
     * 最終学歴
     */
    public static function finalEducation()
    {
    	return array(
    			'' => "",
    			1 => "入学見込み",
    			2 => "卒業",
    			3 => "中退",
    			4 => "在籍中",
    	);
    }
    
    /**
     * 雇用形態
     */
    public static function jobFormId()
    {
    	return array(
    			1 => "正社員",
    			2 => "アルバイト・パート",
    	);
    }
    
    /**
     * 給与形態
     */
    public static function salaryType()
    {
    	return array(
    			1 => "時給",
    			2 => "日給",
    			3 => "月給",
    			4 => "年棒",
    	);
    }
    
    /**
     * お問い合わせ項目
     */
    public static function inquiryCase()
    {
    	return array(
    			'' => "選択してください",
    			1 => "登録について",
    			2 => "サイトの使い方について",
    			3 => "料金について",
    			4 => "当社、運営さいとについて",
    			5 => "ご意見・ご要望について",
    			4 => "その他",
    	);
    }

}