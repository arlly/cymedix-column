<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
    //
	protected $fillable = [
			'title', 'article', 'image', 'status', 'contribute_date'
	];
}
