<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
	
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		
		$this->middleware('guest:admin', ['except' => ['login', 'auth']]);

	}
	
	/**
	 * 認証　
	 * 最終的にはMiddleWareに統合
	 */
	public function authAdmin(){
		if (! \Auth::guard('admin')->check()) {
			return redirect('admin/login');
		}
	}
	
    /**
     * Index
     */
	public function index() { 
		/*
		if (! \Auth::guard('admin')->check()) {
			return redirect('admin/login');
		}
		*/
		return view('admin/index');
	}
	
	/**
	 * login
	 */
	public function login() {
		return view('admin/login');
	}
	
	/**
	 * Auth
	 */
	public function auth(Request $request) {
		$input = $request->all();
		
		$rules = [
				'email'=>'required',
				'password'=>'required',
		];
		 
		$messages = [
				'email.required'=>'email is must.',
				'password.required'=>'password is must.',
		];
		
		$validation = \Validator::make($input,$rules,$messages);
		
		if(\Auth::guard('admin')->attempt(['email'=>$input['email'],'password'=>$input['password']])) {
			return redirect('admin/index');
		} else {
			\Session::flash('flash_message', 'ログインできません');
			return view('admin/login');
		}
	}
	
	/**
	 * Logout
	 */
	public function logout() {
		\Auth::guard('admin')->logout();
		\Session::flash('flash_message', 'ログアウトしました');
		return view('admin/login');
		
	}
	
	
	
	
	
}
