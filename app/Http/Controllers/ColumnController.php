<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Column;

class ColumnController extends Controller
{
    
	/**
	 * コンストラクタ
	 */
	public function __construct(){
	
		$this->middleware('guest:user', ['except' => ['login', 'auth']]);
	
	}
	
	/**
	 * index
	 */
	public function index(){
		$results = Column::latest('contribute_date')->where('contribute_date', '<=', date('Y-m-d H:i:s') )->paginate(1);
		return view('column/index', compact('results'));
	}
		
	/**
	 * listAll
	 */
	public function listAll() {
		$results = Column::latest('contribute_date')->where('contribute_date', '<=', date('Y-m-d H:i:s') )->paginate(10);
		return view('column/list', compact('results'));
	}
	
	/**
	 * detail
	 */
	public function detail(Request $request, $id){
	
		$row = Column::where('contribute_date', '<=', date('Y-m-d H:i:s'))->findOrFail($id);
		return view('column/detail', compact('row'));
	}
}
