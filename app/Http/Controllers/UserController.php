<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
	
	/**
	 * コンストラクタ
	 */
	public function __construct(){
	
		$this->middleware('guest:user', ['except' => ['login', 'auth']]);
	
	}
	
	/**
	 * Index
	 */
	public function index() {
		if (! \Auth::guard('user')->check()) {
			return redirect('user/login');
		}
		return view('user/index');
	}
	
	/**
	 * login
	 */
	public function login() {
		return view('user/login');
	}
	
	
	/**
	 * Logout
	 */
	public function logout() {
		\Auth::guard('user')->logout();
		\Session::flash('flash_message', 'ログアウトしました');
		return view('user/login');
	
	}
	
	/**
	 * Auth
	 */
	public function auth(Request $request) {
		$input = $request->all();
	
		$rules = [
				'email'=>'required',
				'password'=>'required',
		];
			
		$messages = [
				'email.required'=>'email is must.',
				'password.required'=>'password is must.',
		];
	
		$validation = \Validator::make($input,$rules,$messages);
	
		if(\Auth::guard('user')->attempt(['email'=>$input['email'],'password'=>$input['password'], 'status'=>1])) {
			return redirect('user/index');
		} else {
			\Session::flash('flash_message', 'ログインできません');
			return view('user/login');
		}
	}
}
