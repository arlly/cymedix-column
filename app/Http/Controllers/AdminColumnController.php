<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Http\Requests;
use App\Column;

class AdminColumnController extends Controller
{
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		//$this->authAdmin();
		$this->middleware('guest:admin', ['except' => ['login', 'auth']]);
		
		$this->imagePath = base_path() . '/public/images/column/';
	
	
	}
	
	/**
	 * index
	 */
	public function index() {
	
		$results = Column::latest('id')->paginate(10);
		return view('admin/column/index', compact('results'));
	}
	
	/**
	 * add
	 */
	public function add(){
		return view('admin/column/add');
	}
	
	/**
	 * add Post
	 */
	public function store(Request $request){
		$image = "";
		
		$column = new Column();
		
		/** バリデーション **/
		$rules = [
				'title'    => 'required|max:255',
				'image'    => 'mimes:jpg,jpeg,bmp,png|max:100000',
				'article'  => 'max:10000'
		];
		
		$this->validate($request, $rules);
		
		/**
		 * ToDo
		 * バリデーションと画像はpravateメソッドにまとめる
		 */
		if ($request->hasFile('image')) {
			$image = 'column_'. date("YmdHis") . '.' .
			$request->file('image')->getClientOriginalExtension();
			/**
			 * サムネイルを作ります
			 */
			$filepath = \Image::make($request->file('image')->getRealPath());
			$filepath->save($this->imagePath . $image)
			  ->resize(200, null, function ($constraint) {$constraint->aspectRatio();})
			  ->save($this->imagePath .  '200-' . $image);
		
			$request->file('image')->move($this->imagePath, $image);
			$request['image'] = $image;
		}
		
			
		/** DB登録 **/
		$inputs = $request->all();
		$inputs["image"] = $image;
		$column->create($inputs);
		
		\Session::flash('flash_message', '登録しました。');
		return redirect('/admin/column');
		
	}
	
	/**
	 * edit
	 */
	public function edit(Request $request, $id){
		$row = Column::findOrFail($id);
		return view('admin/column/edit', compact('row', 'arrParam'));
	
	}
	
	/**
	 * update
	 */
	public function update(Request $request, $id){
		$image = "";
		
		$column = new Column();
		
		/** バリデーション **/
		$rules = [
				'title'    => 'required|max:255',
				'image'    => 'mimes:jpg,jpeg,bmp,png|max:100000',
				'article'  => 'max:10000'
		];
		
		$this->validate($request, $rules);
		
		if ($request->hasFile('image')) {
			$image = 'column_'. date("YmdHis") . '.' .
			$request->file('image')->getClientOriginalExtension();
			/**
			 * サムネイルを作ります
			 */
			$filepath = \Image::make($request->file('image')->getRealPath());
			$filepath->save($this->imagePath . $image)
			 ->resize(200, null, function ($constraint) {$constraint->aspectRatio();})
			 ->save($this->imagePath .  '200-' . $image);
		
			$request->file('image')->move($this->imagePath, $image);
			$request['image'] = $image;
		}
		
		$inputs = \Request::all();
		$columnsdatas = Column::findOrFail($id);
		
		if ($image)
		  $inputs["image"] = $image;
		else
		  unset($inputs["image"]);
		
		$columnsdatas->update($inputs);
		
		\Session::flash('flash_message', '更新しました。');
		return redirect('/admin/column');
		
	}

	/**
	 * delete
	 */
	public function delete(Request $request, $id){
	
		$column = Column::findOrFail($id);
		$column->delete();
	
		/**
		 * ToDo ファイル削除は絶対パス
		 * 画像削除処理
		 */
		
		if ($column['image']){
			\File::delete("/home/vagrant/public_html/Auth/public/images/column/" . $column['image']
					     ,"/home/vagrant/public_html/Auth/public/images/column/" . "200-". $column['image']);
		}
		\Session::flash('flash_message', $column["title"]. 'を削除しました。');
		return redirect('/admin/column');
	
	}
	
}
