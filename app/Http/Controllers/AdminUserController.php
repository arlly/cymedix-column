<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;


class AdminUserController extends Controller
{
	/**
	 * コンストラクタ
	 */
	public function __construct(){
		//$this->authAdmin();
		$this->middleware('guest:admin', ['except' => ['login', 'auth']]);
		
		
	}
	
	/**
	 * index
	 */
	public function index() {
		
		
		$search = \Session('searchUser');
	
		$results = User::latest('id')
		  ->where('name1', 'like', "%{$search}%")
		  ->orWhere('name2', 'like', "%{$search}%")
		  ->orWhere('email', 'like', "%{$search}%")
		  ->paginate(15);
		
	
		/*$results = User::latest('id')->paginate(15);*/
		return view('admin/user/index', compact('results'));
	}
	
	/**
	 * 検索がクリックされたときの動作
	 */
	public function search(Request $request){
		/** バリデーション **/
		$rules = [
				'search'    => 'required|min:3'
		];
	
		$this->validate($request, $rules);
	
		$request->session()->put('searchUser', $request->search);
		return redirect('/admin/user');
	}
	
	public function add(){
		return view('admin/user/add');
	}
	
	/**
	 * データ登録
	 */
	public function store(Request $request){
		$user = new User();
		/** バリデーション **/
		$rules = [
				'name1'    => 'required|max:255',
				'name2'    => 'required|max:255',
				'email'    => 'required|unique:users|max:255',
				'password' => 'required|max:16|confirmed',
                'password_confirmation' => 'required|min:3|max:16'
		];
	
		$this->validate($request, $rules);
		$request["password"] = \Hash::make($request["password"]);
		
		
		
		/** DB登録 **/
		if ($user->create($request->all()))
		{
			event(new \App\Events\UserWasRegistered($user));
			
			
		}
			
	
		\Session::flash('flash_message', '登録しました。');
		return redirect('/admin/user');
	
	}
	
	/**
	 * edit
	 */
	public function edit(Request $request, $id){
		/**
		 * ToDo
		 * idのnumericチェックを何とかして行う
		 */
		$row = User::findOrFail($id);
		return view('admin/user/edit', compact('row'));
	
	}
	
	/**
	 * update
	 */
	public function update(Request $request, $id) {
		
		/** バリデーション **/
		$rules = [
				'name1'    => 'required|max:255',
				'name2'    => 'required|max:255',
				'email'    => "required|unique:users,email,{$id},id|max:255",
				
		];
		
		$this->validate($request, $rules);
		
		
		$userdatas = User::findOrFail($id);
		//$newsdatas->update($request->all());
		$userdatas->update($request->all());
		
		\Session::flash('flash_message', '更新しました。');
		return redirect('/admin/user');
	}
	
	/**
	 * パスワード変更
	 */
	public function changePassword() {
		return view('admin/user/changePassword');
	}
	
	public function updatePassword(Request $request, $id){
		/** バリデーション **/
		$rules = [
     	  'password' => 'required|min:3|max:16|confirmed',
          'password_confirmation' => 'required|min:3|max:16'
		];
		
		$this->validate($request, $rules);
		
		
		$userdatas = User::findOrFail($id);
		$userdatas->update($request->all());
		
		\Session::flash('flash_message', 'パスワードを更新しました。');
		return redirect('/admin/user');
		
	}
	
	
	
	/**
	 * 認証　
	 * 最終的にはMiddleWareに統合
	 */
	public function authAdmin(){
		if (! \Auth::guard('admin')->check()) {
			return redirect('admin/login');
		}
	}
	
	/**
	 * Test用
	 */
	public function test(){
		return true;
	}
}
