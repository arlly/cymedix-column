<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', function () {
    return view('welcome');
});



Route::group(['middleware' => ['web']], function () {
/**
 * フロントのログイン
 */
  Route::get('user', 'UserController@index');
  Route::get('user/index', 'UserController@index');
  Route::get('user/login', 'UserController@login');
  Route::post('user/login', 'UserController@auth');
  Route::get('user/logout', 'UserController@logout');
  

  
  /**
   * コラム
   */
  Route::get('column',             'ColumnController@index');
  Route::get('column/listAll',     'ColumnController@listAll');
  Route::get('column/{id}/detail', 'ColumnController@detail');
  
  
  /**
   * Admin関連
   */
  Route::get('admin',        'AdminController@index');
  Route::get('admin/index',  'AdminController@index');
  Route::post('admin/login', 'AdminController@auth');
  Route::get('admin/login',  'AdminController@login');
  Route::get('admin/logout', 'AdminController@logout');
  
  /**
   * 管理画面ユーザー関連
   */
  Route::get('admin/user',             'AdminUserController@index');
  Route::post('admin/user',            'AdminUserController@search');
  Route::post('admin/user/add',        'AdminUserController@store');
  Route::get('admin/user/add',         'AdminUserController@add');
  Route::get('admin/user/{id}/edit',   'AdminUserController@edit');
  Route::post('admin/user/{id}/edit',  'AdminUserController@update');
  Route::get('admin/user/{id}/delete', 'AdminUserController@delete');
  
  Route::get('admin/column',             'AdminColumnController@index');
  Route::post('admin/column',            'AdminColumnController@search');
  Route::post('admin/column/add',        'AdminColumnController@store');
  Route::get('admin/column/add',         'AdminColumnController@add');
  Route::get('admin/column/{id}/edit',   'AdminColumnController@edit');
  Route::post('admin/column/{id}/edit',  'AdminColumnController@update');
  Route::get('admin/column/{id}/delete', 'AdminColumnController@delete');
  
  


});
