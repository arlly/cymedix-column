<?php
namespace App\Http\ViewComposers;


use Illuminate\Contracts\View\View;
/*
use Illuminate\Users\Repository as UserRepository;
*/

class ProfileComposer
{
	/**
	 * userリポジトリの実装
	 *
	 * @var UserRepository
	 */
	protected $users;

	/**
	 * 新しいプロフィールコンポーサーの生成
	 *
	 * @param  UserRepository  $users
	 * @return void
	 */
	public function __construct()
	{
		// 依存はサービスコンテナにより自動的に解決される…
		//$this->users = $users;
	}

	/**
	 * データをビューと結合
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$view->with('count', 30000);
	}
}