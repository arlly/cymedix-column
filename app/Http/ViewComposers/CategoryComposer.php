<?php
namespace App\Http\ViewComposers;


use Illuminate\Contracts\View\View;
use App\Job;
use App\Feature;

/*
use Illuminate\Users\Repository as UserRepository;
*/

class CategoryComposer
{


	/**
	 * 新しいプロフィールコンポーサーの生成
	 *
	 * @param  UserRepository  $users
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * データをビューと結合
	 *
	 * @param  View  $view
	 * @return void
	 */
	public function compose(View $view)
	{
		$arr_jobs = Job::all();
		$view->with('arr_jobs', $arr_jobs);
		
		$arr_features = Feature::all();
		$view->with('arr_features', $arr_features);
		
		$arrParam   = \Config::get('Fixed');
		$view->with('arr_param', $arrParam);
		
		$arrNowJob = \Fixed::nowJob();
		$view->with('nowJob', $arrNowJob);
		
		
	}
}