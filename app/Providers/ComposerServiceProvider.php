<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
    	// クラスベースのコンポーサーを使用する
    	view()->composer(
    			'*', 'App\Http\ViewComposers\CategoryComposer'
    			);
    	
    	// クロージャーベースのコンポーサーを使用する
    	view()->composer('dashboard', function ($view) {
    	
    	});
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
