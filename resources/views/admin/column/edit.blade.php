@extends('layouts.admin')

@section('content')
<div class="container">
  <h2>コラム追加</h2>
  
  {{-- エラーの表示を追加 --}}
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    
    
    
  {!! Form::open(array('files' => true)) !!}
  
    <div class="form-group">
      {!! Form::label('title', 'タイトル:') !!}
      {!! Form::text('title', $row->title, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
    @if ($row->image)
      <img src="/images/column/200-{{$row->image}}">
    @endif
      {!! Form::label('image', '画像:') !!}
      {!! Form::file('image', null) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('article', '本文:') !!}
      {!! Form::textarea('article', $row->article, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    <div class="form-group">
      {!! Form::label('contribute_date', '投稿日時:') !!}
      {!! Form::text('contribute_date', $row->contribute_date, ['class' => 'form-control ckeditor']) !!}
    </div>
    
    
    <!--  
    <div class="form-group">
      {!! Form::label('ステータス', 'ステータス:') !!}
    
      {!! Form::radio('status', '1', true) !!}有効　
      {!! Form::radio('status', '0') !!}　無効
    </div>
    -->
    
    
    <div class="form-group">
      {!! Form::submit('送信', ['class' => 'btn btn-primary form-control']) !!}
    </div>
  
  {!! Form::close() !!}
</div>
@endsection
