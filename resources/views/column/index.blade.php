@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">       
        {!! $results->render() !!}
        
        @foreach($results as $result)
        <h3>{{ $result->title }}</h3>
        
        
        
        
        <div class="well">
        @if ($result->image)
            <p><img src="/images/column/{{$result->image}}" style="width:100%;"></p>
        @endif
        <p>{!! nl2br($result->article) !!}</p>
        </div>
        
        <div class="well">
        <p>{{ $result->contribute_date }}</p>
        </div>
        
        @endforeach
 
            
            
        </div>
    </div>
</div>
@endsection
