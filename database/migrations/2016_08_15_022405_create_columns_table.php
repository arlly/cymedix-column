<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateColumnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
    	Schema::create('columns', function (Blueprint $table) {
    	  $table->increments('id');
    	  $table->string('title', 255);        
    	  $table->text('article');
    	  $table->string('image', 255)->nullable();
    	  $table->tinyInteger('status')->default(1);
    	  $table->dateTime('contribute_date')->default(DB::raw('CURRENT_TIMESTAMP'));
    	  $table->timestamps();
    	  
    	});
    	
    	
    	
    	
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
